
# Zola-Inky

> An elegant and understated theme for Zola

Zola Inky ([view demo](https://jimmyff.github.io/zola-inky)) is a theme by [jimmyff](https://github.com/jimmyff) and [mr-karan](https://github.com/mr-karan) for the [Zola](https://www.getzola.org/) static site generator. This theme was originally based on the [hugo-ink](https://github.com/knadh/hugo-ink) theme, ported by mr-karan. It was then packaged and developed further by jimmyff. The theme is available on [Github](https://github.com/jimmyff/zola-inky) under the MIT license, for more information on how to use it please see the [readme](https://github.com/jimmyff/zola-inky/blob/main/README.md) and check the [changelog](https://github.com/jimmyff/zola-inky/blob/main/CHANGELOG.md) for a list of the latest changes.

[![PNG](screenshot.png)](https://jimmyff.github.io/zola-inky)

## Changes compared to the original

The main objective of the fork is to have no external dependencies (Google fonts, CDN...).
It also adds flexibility.

### Stop using Google fonts

The required fonts are loaded locally from the theme itself.

### Use fork-awesome for icons

Fork-awesome is used instead of feather, because it has more icons (e.g. a proper Mastodon icon).

In addition, all icons are loaded locally from the theme itself.

### Override footer content

You can override the content of the footer by overriding `base.html`.
There are three blocks available:

- `footer_left`: contains social and RSS buttons by default
- `footer_center`: empty by default
- `footer_right`: contains attributions by default (please keep the link to the original theme author)

Here is an example that you can put in `templates/base.html` in your website:

```html
{% extends "zola-inky/templates/base.html" %}

{% block footer_center %}
<a target="_blank" href="https://deuxfleurs.fr/"><img src="{{ get_url(path='deuxfleurs_en.png') }}" alt="Badge showing the website is hosted by Deuxfleurs" title="Website hosted by Deuxfleurs"></a>
{% endblock footer_center %}

{% block footer_right %}
Licence: CC-BY-SA {{ config.title }} {{ now() | date(format="%Y") }}
<br>
Powered by <a target="_blank" href="https://getzola.org/">Zola</a>.
Theme: <a target="_blank" href="https://github.com/jimmyff/zola-inky">Inky</a> (<a href="https://git.deuxfleurs.fr/baptiste/zola-inky-fork">fork</a>).
{% endblock footer_right %}
```

### Styling for tables

See <https://blog.bitsofnetworks.org/riscv-performance-power-usage/>
